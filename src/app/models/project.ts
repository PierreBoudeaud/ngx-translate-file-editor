export type Project = {
    id: string;
    name: string;
    path: string;
    locales: string[];
    lastModification: number;
}
