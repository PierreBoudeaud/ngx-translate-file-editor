import { Component, inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogClose, MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatFormField, MatLabel } from '@angular/material/form-field';
import { MatButton, MatIconButton } from '@angular/material/button';
import { ProjectInput } from '@services/projet.service';
import { MatInput } from '@angular/material/input';

@Component({
  selector: 'app-project-form',
  standalone: true,
  imports: [
    MatFormField,
    ReactiveFormsModule,
    MatIconButton,
    MatLabel,
    MatDialogClose,
    MatButton,
    MatInput,
  ],
  templateUrl: './project-form.component.html',
  styleUrl: './project-form.component.css'
})
export class ProjectFormComponent implements OnInit {
  private readonly data = inject<{ path: string }>(MAT_DIALOG_DATA);
  private readonly fb = inject(FormBuilder);
  private readonly dialogRef = inject<MatDialogRef<ProjectFormComponent, ProjectInput | undefined>>(MatDialogRef)

  readonly formGroup = this.fb.group({
    path: this.fb.nonNullable.control<string>(''),
    name: this.fb.nonNullable.control<string>('', Validators.minLength(1)),
  });

  ngOnInit() {
    this.formGroup.patchValue({
      path: this.data.path,
    });
  }

  submit() {
    if (this.formGroup.valid) {
      const { name, path } = this.formGroup.value;
      this.dialogRef.close(<ProjectInput> {
        path: path as string,
        name: name as string,
      });
    }
  }
}
