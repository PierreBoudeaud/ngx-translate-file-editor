import { Component, inject } from '@angular/core';
import { ProjetService } from '@services/projet.service';
import { ProjectDetailComponent } from '@components/project-detail/project-detail.component';
import { AsyncPipe } from '@angular/common';
import { MatIconButton } from '@angular/material/button';
import { MatIcon } from '@angular/material/icon';
import { Project } from '@models/project';
import { Router } from '@angular/router';
import { combineLatest, debounceTime, filter, map, shareReplay, startWith, switchMap } from 'rxjs';
import { open } from '@tauri-apps/api/dialog';
import { fromPromise } from 'rxjs/internal/observable/innerFrom';
import { MatDialog } from '@angular/material/dialog';
import { ProjectFormComponent } from '@components/projects-list/project-form/project-form.component';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';

@Component({
    selector: 'app-projects-list',
    standalone: true,
    imports: [
        ProjectDetailComponent,
        AsyncPipe,
        MatIconButton,
        MatIcon,
        ReactiveFormsModule,
    ],
    templateUrl: './projects-list.component.html',
    styleUrl: './projects-list.component.scss',
})
export class ProjectsListComponent {
    private readonly projectService = inject(ProjetService);
    private readonly router = inject(Router);
    private readonly dialog = inject(MatDialog);
    private readonly fb = inject(FormBuilder);

    control = this.fb.nonNullable.control<string>('');

    readonly allProjects$ = this.projectService.getProjects();

    readonly projects$ = combineLatest({
        filter: this.control.valueChanges.pipe(
            debounceTime(250),
            map(filter => filter.toUpperCase()),
            startWith(''),
        ),
        projects: this.allProjects$,
    }).pipe(
        map(({ filter, projects }) => {
            let filteredProjects: Project[];
            if (filter !== '') {
                filteredProjects = projects.filter(project => project.name.toUpperCase().includes(filter));
            } else {
                filteredProjects = projects;
            }
            return filteredProjects.sort(this.comparator);
        }),
        shareReplay(1),
    );

    goToProject(project: Project) {
        this.router.navigate([ '/', 'project', project.id ]);
    }

    deleteProject(project: Project) {
        this.projectService.deleteProject(project);
    }

    createProject() {
        fromPromise(open({
            title: 'Sélectionnez le dossier contenant les fichiers de traduction.',
            directory: true,
            defaultPath: '',
        })).pipe(
            map(path => {
                if (typeof path != 'string') {
                    return null;
                } else {
                    return path;
                }
            }),
            filter(path => !!path),
            switchMap(path => this.dialog.open<ProjectFormComponent, { path: string }, Project | undefined>(
                ProjectFormComponent,
                {
                    data: {
                        path: path as string,
                    },
                    width: '50%',
                    height: '50%',
                },
            ).afterClosed()),
        ).subscribe({
            next: project => {
                if (!project) {
                    return;
                }
                const { id } = this.projectService.createProject(project);
                this.router.navigate(['/', 'project', id]);
            },
        });
    }

    comparator(a: Project, b: Project): number {
        const aModification = a.lastModification;
        const bModification = b.lastModification;

        if (bModification < aModification) {
            return -1;
        } else if (bModification > aModification) {
            return 1;
        } else {
            return 0;
        }
    }
}
