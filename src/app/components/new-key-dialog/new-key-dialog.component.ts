import { Component, inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogClose, MatDialogRef, MatDialogTitle } from '@angular/material/dialog';
import { FormBuilder, FormControl, FormRecord, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatFormField, MatLabel } from '@angular/material/form-field';
import { MatInput } from '@angular/material/input';
import { MatButton } from '@angular/material/button';
import { first, Observable } from 'rxjs';
import { Locale } from '@services/current-file-content-state-service.service';

@Component({
    selector: 'app-new-key-dialog',
    standalone: true,
    imports: [
        MatDialogTitle,
        ReactiveFormsModule,
        MatDialogClose,
        MatFormField,
        MatLabel,
        MatInput,
        MatButton,
    ],
    templateUrl: './new-key-dialog.component.html',
    styleUrl: './new-key-dialog.component.scss',
})
export class NewKeyDialogComponent implements OnInit {
    private fb = inject(FormBuilder);
    private dialogRef = inject<MatDialogRef<NewKeyDialogComponent, string | undefined>>(MatDialogRef);
    protected $locales = inject<Observable<Locale[]>>(MAT_DIALOG_DATA);

    readonly formGroup = this.fb.group({
        keyName: this.fb.nonNullable.control<string>('', [
            Validators.required,
            Validators.minLength(1),
            Validators.pattern('^(?![.])([a-zA-Z0-9]+(\\.[a-zA-Z0-9]+)*)(?![.])$'),
        ]),
        translations: this.fb.nonNullable.record<FormRecord<FormControl<string>>>({}),
    });

    ngOnInit() {
        this.$locales.pipe(
            first(),
        ).subscribe({
            next: locales => {
                locales.forEach(locale => {
                   const translationsRecord: FormRecord = this.formGroup.get('translations') as FormRecord;
                   translationsRecord.addControl(locale, this.fb.nonNullable.control<string>(''))
                });
            }
        });
    }

    submit() {
        if (this.formGroup.valid) {
            this.dialogRef.close(this.formGroup.value.keyName);
        }
    }
}
