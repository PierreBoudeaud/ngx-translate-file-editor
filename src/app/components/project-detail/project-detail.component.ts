import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Project } from '@models/project';
import { MatIcon } from '@angular/material/icon';

@Component({
  selector: 'app-project-detail',
  standalone: true,
    imports: [
        MatIcon,
    ],
  templateUrl: './project-detail.component.html',
  styleUrl: './project-detail.component.scss'
})
export class ProjectDetailComponent {
  @Input({
    required: true
  })
  project!: Project;
  @Output()
  goToProject$ = new EventEmitter<Project>();
  @Output()
  deleteProject$ = new EventEmitter<Project>();

  deleteProject(event: MouseEvent) {
    event.stopPropagation();
    this.deleteProject$.emit(this.project);
  }
}
