import { Component, Input, Output } from '@angular/core';
import { Key, Locale, Translation } from '@services/current-file-content-state-service.service';
import { AsyncPipe, NgIf } from '@angular/common';
import { MatFormField, MatLabel } from '@angular/material/form-field';
import { MatInput } from '@angular/material/input';
import { Observable, Subject } from 'rxjs';
import { MatIconButton } from '@angular/material/button';
import { MatIcon } from '@angular/material/icon';

@Component({
  selector: 'app-translation-detail',
  standalone: true,
  imports: [
    AsyncPipe,
    MatFormField,
    MatInput,
    MatLabel,
    NgIf,
    MatIconButton,
    MatIcon,
  ],
  templateUrl: './translation-detail.component.html',
  styleUrl: './translation-detail.component.css'
})
export class TranslationDetailComponent {
  @Input({
    required: true,
  })
  key!: Key;
  @Input({
    required: true,
  })
  translation!: Translation;
  @Input({
    required: true,
  })
  $locales!: Observable<Locale[]>;
  @Output()
  $delete = new Subject<Key>();
}
