import { Component } from '@angular/core';
import { ProjectsListComponent } from '@components/projects-list/projects-list.component';

@Component({
  selector: 'app-projets',
  standalone: true,
  imports: [
    ProjectsListComponent,
  ],
  templateUrl: './projets.component.html',
  styleUrl: './projets.component.css'
})
export class ProjetsComponent {

}
