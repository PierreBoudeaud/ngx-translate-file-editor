import { Component, inject, OnInit } from '@angular/core';
import { CommonModule, NgOptimizedImage } from '@angular/common';
import { ActivatedRoute, RouterLink, RouterOutlet } from '@angular/router';
import { invoke } from '@tauri-apps/api/tauri';
import { open } from '@tauri-apps/api/dialog';
import {
    CurrentFileContentStateServiceService,
    Key,
    TranslationFile,
    Translations,
} from '@services/current-file-content-state-service.service';
import {
    combineLatest,
    debounceTime,
    EMPTY,
    filter,
    first,
    map,
    Observable,
    shareReplay,
    startWith,
    switchMap,
} from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { NewKeyDialogComponent } from '@components/new-key-dialog/new-key-dialog.component';
import { MatFormField, MatLabel } from '@angular/material/form-field';
import { MatInput } from '@angular/material/input';
import { MatIcon } from '@angular/material/icon';
import { MatIconButton } from '@angular/material/button';
import { TranslationDetailComponent } from '@components/translation-detail/translation-detail.component';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { MatCheckbox } from '@angular/material/checkbox';
import { CdkFixedSizeVirtualScroll, CdkVirtualForOf, CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import { ProjetService } from '@services/projet.service';
import { fromPromise } from 'rxjs/internal/observable/innerFrom';
import { Project } from '@models/project';


@Component({
    selector: 'app-project',
    standalone: true,
    imports: [
        CommonModule,
        RouterOutlet,
        MatFormField,
        MatLabel,
        MatInput,
        MatIcon,
        MatIconButton,
        NgOptimizedImage,
        TranslationDetailComponent,
        ReactiveFormsModule,
        MatCheckbox,
        CdkVirtualScrollViewport,
        CdkFixedSizeVirtualScroll,
        CdkVirtualForOf,
        RouterLink,
    ],
    templateUrl: './project.component.html',
    styleUrl: './project.component.css',
})
export class ProjectComponent implements OnInit {
    private readonly projectService = inject(ProjetService);
    private readonly currentFileContentStateServiceService = inject(CurrentFileContentStateServiceService);
    private readonly dialog = inject(MatDialog);
    private readonly fb = inject(FormBuilder);
    private readonly activatedRoute = inject(ActivatedRoute);

    readonly searchControl = this.fb.nonNullable.control<string>('');
    readonly hideGoodTranslationsControl = this.fb.nonNullable.control<boolean>(false);
    readonly $filter: Observable<string> = this.searchControl.valueChanges.pipe(
        debounceTime(500),
        map(filter => filter.toUpperCase()),
        startWith(''),
    );
    readonly $files: Observable<TranslationFile[]> = this.currentFileContentStateServiceService.getState();
    readonly $locales: Observable<string[]> = this.$files.pipe(
        map(translationFiles => translationFiles.map(({ locale }) => locale)),
        startWith([]),
        shareReplay(1),
    );
    readonly $working = combineLatest({
        working: this.currentFileContentStateServiceService.getCurrentWork(),
        filter: this.$filter,
        hideGoodTranslations: this.hideGoodTranslationsControl.valueChanges.pipe(startWith(false)),
        locales: this.$locales,
    }).pipe(
        map(({ working, filter, hideGoodTranslations, locales }) => {
            let processTranslations: Translations;
            let finalTranslations: Translations;
            if (filter === '') {
                processTranslations = working;
            } else {
                processTranslations = {};
                const allKeys = Object.keys(working);
                const filteredKeys = allKeys.filter(key => {
                    const upperKey = key.toUpperCase();
                    return upperKey.includes(filter);
                });

                filteredKeys.forEach(key => {
                    processTranslations[key] = working[key];
                });
            }

            if (!hideGoodTranslations) {
                finalTranslations = processTranslations;
            } else {
                finalTranslations = {};
                Object.entries(processTranslations)
                    .filter(([ key, translations ]) => {
                        const translationlocales = Object.keys(translations);
                        return locales.some(locale => !translationlocales.includes(locale));
                    })
                    .forEach(([ key, translations ]) => {
                        finalTranslations[key] = translations;
                    });
            }

            return finalTranslations;
        }),
    );

    project$? : Observable<Project | undefined>;

    path?: string;

    ngOnInit() {
        this.project$ = this.activatedRoute.paramMap.pipe(
            map(params => params.get('id')),
            switchMap(id => this.projectService.getProject(id)),
            first(),
            shareReplay(1),
        );
        combineLatest({
            id: this.project$.pipe(
                map(project => project?.id),
            ),
            locales: this.project$.pipe(
                map(project => project?.path),
                switchMap(path => this.refreshAll(path)),
                map(translationsFiles => translationsFiles.map(({ locale }) => locale)),
            )
        }).subscribe({
            next: ({ id, locales }) => {
                if (!id) {
                    return;
                }
                this.projectService.refreshProjectLocale(id, locales);
            }
        });
    }

    async listI18nFiles() {
        const path = await open({
            title: 'Sélectionnez le dossier contenant les fichiers de traduction.',
            directory: true,
            defaultPath: '',
        });
        if (!path || typeof path !== 'string') {
            return;
        }
        this.path = path;
        this.refreshAll(path);
    }

    refreshAll(path?: string) {
        if (!path) {
            return EMPTY;
        }
        const translationsPromise$ = fromPromise<TranslationFile[]>(invoke<TranslationFile[]>('scan_i18n_files', { path }));

        translationsPromise$.subscribe({
           next: files => this.currentFileContentStateServiceService.setState(files),
        });

        return translationsPromise$;
    }

    createKey() {
        this.dialog.open<NewKeyDialogComponent, void, string | undefined>(
            NewKeyDialogComponent,
        ).afterClosed()
            .pipe(
                filter(key => typeof key === 'string' && key !== '' && key != ' '),
                map(key => key as string),
            ).subscribe({
            next: key => this.currentFileContentStateServiceService.addKey(key),
        });
    }

    deleteKey(key: Key) {
        this.currentFileContentStateServiceService.deleteKey(key);
    }
}
