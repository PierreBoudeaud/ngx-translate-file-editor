import { Routes } from '@angular/router';

export const routes: Routes = [
    {
        path: 'projects',
        loadComponent: () => import('@routes/projets/projets.component').then(c => c.ProjetsComponent),
    },
    {
        path: 'project/:id',
        loadComponent: () => import('@routes/project/project.component').then(c => c.ProjectComponent),
    },
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'projects',
    }
];
