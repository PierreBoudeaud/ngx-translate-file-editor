import { Injectable } from '@angular/core';
import { Project } from '@models/project';
import { EMPTY, first, map, Observable, shareReplay, startWith, Subject } from 'rxjs';
import { v4 as uuid } from 'uuid';

const PROJECTS_KEY = 'data/projects';

export type ProjectInput = Omit<Project, 'id' | 'locales' | 'lastModification'>;

@Injectable({
  providedIn: 'root'
})
export class ProjetService {
  private onUpdate$ = new Subject<Project[]>();

  private projects$ = this.onUpdate$.pipe(
      startWith(this.loadProjects()),
      shareReplay(1),
  );

  private loadProjects(): Project[] {
    const localProjects = localStorage.getItem(PROJECTS_KEY) ?? '[]';
    return JSON.parse(localProjects);
  }

  getProjects(): Observable<Project[]> {
    return this.projects$;
  }

  saveProjects(projects: Project[]) {
    this.onUpdate$.next(projects);
    const localProjects = JSON.stringify(projects);
    localStorage.setItem(PROJECTS_KEY, localProjects);
  }

  getProject(id: string | undefined | null): Observable<Project | undefined> {
    if (!id) {
      return EMPTY;
    }
    return this.projects$.pipe(
        map(projects => projects.find(project => project.id === id)),
    );
  }

  saveProject(projectToSave: Project) {
    projectToSave.lastModification = (new Date()).getTime();
    const projects = this.loadProjects();
    this.saveProjects([...projects.filter(project => project.id !== projectToSave.id), projectToSave]);
  }

  createProject(projectToSave: ProjectInput) {
    const project = <Project> {
      ...projectToSave,
      id: uuid(),
      locales: [],
      lastModification: (new Date()).getTime(),
    }
    this.saveProject(project);
    return project;
  }

  refreshProjectLocale(id: string, locales: string[]) {
    this.getProject(id).pipe(
        first(),
    ).subscribe({
      next: project => {
        if (!project) {
          return;
        }
        project.locales = locales.sort();
        this.saveProject(project);
      }
    });
  }

  deleteProject({ id }: Project) {
    this.getProjects().pipe(
        first(),
        map(projects => projects.filter(project => project.id !== id)),
    ).subscribe({
      next: projects => this.saveProjects(projects),
    });
  }
}
