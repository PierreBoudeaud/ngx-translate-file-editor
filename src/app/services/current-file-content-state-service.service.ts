import { Injectable } from '@angular/core';
import { BehaviorSubject, combineLatest, first, map, merge, Observable, shareReplay, Subject } from 'rxjs';

export type Locale = string;
export type TranslationValue = string;
export type Key = string;

export type TranslationFile = {
  locale: string,
  content: string,
}

export type Translations = Record<Key, Record<Locale, TranslationValue>>;
export type Translation = Record<Locale, TranslationValue>;

@Injectable({
  providedIn: 'root'
})
export class CurrentFileContentStateServiceService {
  private $translationFiles = new BehaviorSubject<TranslationFile[]>([]);

  private $saveWorkingState = new Subject<Translations>();
  private $currentWorkingState = merge(
    this.$translationFiles.pipe(
      map(translationFiles => this.convertTranslationFilesToTranslations(translationFiles)),
    ),
    this.$saveWorkingState,
  ).pipe(
    shareReplay(1),
  );

  setState(translationFiles: TranslationFile[]): void {
    this.$translationFiles.next(translationFiles);
  }

  getState(): Observable<TranslationFile[]> {
    return this.$translationFiles.asObservable();
  }

  getCurrentState(): TranslationFile[] {
    return this.$translationFiles.value;
  }

  getCurrentWork(): Observable<Translations> {
    return this.$currentWorkingState;
  }

  convertTranslationFilesToTranslations(translationFiles: TranslationFile[]): Translations {
    const translations: Translations = {};
    const translationsFilesParsed = translationFiles.map(({ locale, content }) => ({ locale, content: JSON.parse(content) }));

    translationsFilesParsed.forEach(({ locale, content }) => {
      this.convert(translations, locale, '', content);
    });

    return translations;
  }

  convert(translations: Translations, locale: string, oldPath: string, objectValue: any): void {
    Object.entries(objectValue).forEach(([key, value]) => {
      const currentPath = oldPath !== '' ? `${oldPath}.${key}` : key;
      if (typeof value === 'string') {
        let translation: Translation;
        const existingTranslation: Translation | undefined = translations[currentPath];
        if (existingTranslation) {
          translation = existingTranslation;
        } else {
          translation = {};
          translations[currentPath] = translation;
        }
        translation[locale] = value;
      } else {
        this.convert(translations, locale, currentPath, value);
      }
    });
  }

  addKey(key: string) {
    combineLatest({
      locales: this.$translationFiles.pipe(
        map(translationsFiles => translationsFiles.map(({ locale }) => locale)),
      ),
      translations: this.$currentWorkingState,
    }).pipe(
      first(),
    ).subscribe({
      next: ({ locales, translations }) => {
        const translation: Translation = {};
        translations[key] = translation;
        locales.forEach(locale => translation[locale] = '');
        this.$saveWorkingState.next(translations);
      }
    });
  }

  deleteKey(key: Key) {
    this.$currentWorkingState.pipe(
        first(),
    ).subscribe({
      next: translations => {
       delete translations[key];
       this.$saveWorkingState.next(translations);
      }
    });
  }
}
