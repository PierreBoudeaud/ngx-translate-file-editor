// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

use std::fs::{read_dir, read};

use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize)]
struct TranslationFile {
    locale: String,
    content: String,
}

// Learn more about Tauri commands at https://tauri.app/v1/guides/features/command
#[tauri::command]
fn greet(name: &str) -> String {
    format!("Hello, {}! You've been greeted from Rust!", name)
}

#[tauri::command]
fn scan_i18n_files(path: &str) -> Vec<TranslationFile> {
    let mut files = Vec::new();
    let folder_files = read_dir(&path)
        .expect("Error during reading folder");
    for optional_entry in folder_files {
        let entry = optional_entry
            .expect("Failed to get file");
        let file_name = entry.file_name().into_string()
            .expect("Fail to convert OsString to String");
        let file_metadata = entry.metadata()
            .expect("Failed to get metadata of entry");
        if file_metadata.is_file() {
            let file_content = read(entry.path())
                .expect("Failed to read file");
            let content: String = String::from_utf8(file_content)
                .expect("Failed to convert file content to String");
            let locale: &str = file_name.split('.').next().expect("Failed to get Locale from fileName");
            files.push(TranslationFile { locale: String::from(locale), content: content });
        }
    }
    files
}

/*#[tauri::command]
fn folder_exist(path: &str) -> bool {
    match read_dir(path) {
        Some(_e) => true,
        None => false,
    }
}*/

fn main() {
    tauri::Builder::default()
        .invoke_handler(tauri::generate_handler![greet])
        .invoke_handler(tauri::generate_handler![scan_i18n_files])
        //.invoke_handler(tauri::generate_handler![folder_exist])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
