# NGX Translate File Editor

Manage and debug your translation file for the translation library NgxTranslate for Angular Framework.

## Requirements
- [Tauri prerequisites](https://tauri.app/v1/guides/getting-started/prerequisites)
- Node : 20 ([nvm](https://github.com/nvm-sh/nvm) recommended)
- Rust: 1.75.0

## Current features
- **Project dashboard**: Display a list of projects and provide quick access to their translation files
- **Translation file management (partial)**: Allow users to add, edit, delete, or import/export translation files.
- Filter by translation key
- Filter by incomplete translations

## Not implemented yet
- **Write mode**: Modify the translations and update json files
- **UX/UI improvements**: Improve UI for better user experience


## Maybe implemented in the future
- **Version Control**: Add versions to roll back at a specific point
- **AI auto translate**: Add AI to help for quickly translate a file
- **XLF files support**: Support of XLF file import/export and/or convert to json

## Recommended IDE Setup

[VS Code](https://code.visualstudio.com/) + [Tauri](https://marketplace.visualstudio.com/items?itemName=tauri-apps.tauri-vscode) + [rust-analyzer](https://marketplace.visualstudio.com/items?itemName=rust-lang.rust-analyzer) + [Angular Language Service](https://marketplace.visualstudio.com/items?itemName=Angular.ng-template).
